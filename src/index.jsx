import { useState, useEffect } from 'react'

const data = {
    className: '', // 自定义样式

    prizeId: 1, // 中奖ID
    defaultPrizeId: 2, // 异常场景下的奖品ID，没有则异常不高亮奖品
    readyToRun: true, // 是否开启滚动
    
    speedFrom: 50, // ms
    speedTo: 400, // ms
    duration: 3000, // ms
    timeFunction: '', // enum or bezier-easing

    prizeList: [{
        prizeId: 1,
        image: '',
        imageHightlight: '',
        render: function(isHighlight, prize, currentIndex) {
            return 
        }
    }]
}

export function Lottery(props) {
    const { prizeList, prizeId, readyToRun} = props;
    const prizeIndex = prizeList.findIndex(prize => prize.prizeId === prizeId);
    const [currentIndex, setCurrentIndex] = useState(prizeIndex);
    
    useEffect(() => {
        startRun();
        
        return () => {
            stopRun();
        }
    }, [readyToRun]);

    function startRun() {

    }

    return (
        <div>
            {
                prizeList.map((prize, index) => {
                    const { render, image, imageHightlight } = prize;
                    const isHighlight = currentIndex === index;
                    
                    if(render) {
                        return render(isHighlight, prize, index);
                    }

                    if(!!imageHightlight || !!image) {
                        return isHighlight ? imageHightlight : image
                    }

                    return null;                    
                })
            }
        </div>
    )
}